# signal-klieh

signal-klieh is a wrapper around [signal-cli](https://github.com/AsamK/signal-cli).

## Installation
First install [signal-cli](https://github.com/AsamK/signal-cli). Then you can install signal-klieh:
```
git clone https://gitlab.com/mo_krauti/signal-klieh.git
cd signal-klieh
python setup.py install
```

## Usage
First register or link your account using [signal-cli](https://github.com/AsamK/signal-cli).
You can 
