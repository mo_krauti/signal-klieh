from setuptools import setup

setup(
        name='signal-klieh',
        version='0.1',
        packages=['signalklieh'],
        scripts=['scripts/signal-klieh'],
        install_requires=['click', 'sqlalchemy']
        )
