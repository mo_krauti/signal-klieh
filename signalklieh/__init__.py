import os
from pathlib import Path

config_dir = str(Path.home())+'/.local/share/signal-klieh/'
if os.path.isdir(config_dir) == False:
    os.mkdir(config_dir)

import signalklieh.db
import signalklieh.config
import signalklieh.signal

