import json
from os.path import isfile
from pathlib import Path
from signalklieh.helper import error

config_dir = str(Path.home())+'/.local/share/signal-klieh/'

if isfile(config_dir+'config.json') == False:
    config = {'phone_number':input('Please enter your phone number: '), 'contacts':{'+123456789':'contact_name', 'group_base64':'group_name'}}
    with open(config_dir+'config.json', 'w') as config_file:
        json.dump(config, config_file, sort_keys=True, indent=4)


#Load config
with open(config_dir+'config.json', 'r') as config_file:
    config = json.load(config_file)

#def write():
#    with open(config_dir+'config.json', 'w') as config_file:
#        json.dump(config, config_file, sort_keys=True, indent=4)
def get_con(num): #Converts number to contact Name if available, else returns num again
    if num in config['contacts']:
        return config['contacts'][num]
    else:
        return num
def get_num(con): #Converts contact name to number
    for number, name in config['contacts'].items():
        if name == con:
            return number
    error('There is no contact with the name: '+con)
