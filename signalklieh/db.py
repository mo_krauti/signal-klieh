from os.path import isfile
from pathlib import Path
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy import create_engine

config_dir = str(Path.home())+'/.local/share/signal-klieh/'

Base = declarative_base()

class Message(Base):
    __tablename__ = 'messages'
    id = Column(Integer, primary_key=True)
    timestamp = Column(Integer) #unix epoch seconds utc
    source = Column(String(20))
    text = Column(String(500))
    target = Column(String(30))
    def __repr__(self):
        return str([self.id, self.timestamp, self.source, self.text, self.target])


engine = create_engine('sqlite:///'+config_dir+'msgs.db')
if isfile(config_dir+'msgs.db') == False:
    Base.metadata.create_all(engine)
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()

def add(obj):
    session.add(obj)
    session.commit()

