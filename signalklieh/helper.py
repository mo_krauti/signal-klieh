import subprocess

def run_process(arg_list):
    p = subprocess.run(arg_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if p.returncode != 0:
        error('Command: '+str(arg_list)+'\n stdout: '+p.stdout.decode()+'\n stderr: '+p.stdout.decode()+'\n returncode: '+str(p.returncode))
    return p.stdout.decode()

def error(desc):
    print("An error occured:")
    print(desc)
    exit()
