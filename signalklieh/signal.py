import json
import time
import signalklieh.config
import signalklieh.db
from signalklieh.helper import run_process

NUM = signalklieh.config.config['phone_number']

def get_msgs():
    json_str = '['+run_process(['signal-cli', '-u', NUM, 'receive', '--json']).replace('\n',',')[:-1]+']'
    msgs = json.loads(json_str)
    parsed_msgs = []
    for i in msgs:
        if i['envelope']['dataMessage'] != None:
            timestamp = int(str(i['envelope']['timestamp'])[:-3]) #unix epoch seconds utc
            source = i['envelope']['source']
            text = i['envelope']['dataMessage']['message']
            if i['envelope']['dataMessage']['groupInfo'] != None:
                msg = signalklieh.db.Message(timestamp=timestamp, source=source, text=text, target=i['envelope']['dataMessage']['groupInfo']['groupId'])
            else:
                msg = signalklieh.db.Message(timestamp=timestamp, source=source, text=text, target=NUM)
            parsed_msgs.append(msg)
            signalklieh.db.add(msg)
    return parsed_msgs

def send_msg(num, text, group=False):
    timestamp = int(time.time())
    if group:
        lst = ['signal-cli', '-u', NUM, 'send', '-g', num, '-m', text]
    else:
        lst = ['signal-cli', '-u', NUM, 'send', '-m', text, num]
    run_process(lst)
    msg = signalklieh.db.Message(timestamp=timestamp, source=NUM, text=text, target=num)
    signalklieh.db.add(msg)
